package com.gocybered.royalcarpet.model;

import com.gocybered.royalcarpet.commands.CommandEnable;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Eddie on 3/19/2016.
 */
public class RestorePlayerBlocks {
    private Player player;
    private Location location;
    private Set<RestoreBlock> restoreBlocks = new HashSet<>();

    public RestorePlayerBlocks(Player player){
        this.player = player;
    }

    public RestorePlayerBlocks(Player player, Location location){
        this.player = player;
        this.location = location;
    }

    public void addBlock(Block block){
        RestoreBlock rb = new RestoreBlock(block);
        restoreBlocks.add(rb);
    }

    public void setLocation(Location location){
        this.location = location;
    }
    public Location getLocation() {
        return location;
    }

    public boolean isPotentialOverlap(Location otherLoc){
        //TODO: Improve bounds check
        return otherLoc.getBlockY() == location.getBlockY()
                && otherLoc.getBlockX() < location.getBlockX() + 4
                && otherLoc.getBlockX() > location.getBlockX() - 4
                && otherLoc.getBlockZ() < location.getBlockZ() + 4
                && otherLoc.getBlockZ() > location.getBlockZ() - 4;
    }

    public boolean isOverlap(RestorePlayerBlocks otherRpb){
        for(RestoreBlock rb : otherRpb.restoreBlocks){
            if( isOverlap(rb.getLocation()) ){
                return true;
            }
        }
        return false;
    }

    public boolean isOverlap(Location otherLoc){
        return otherLoc.getBlockX() == location.getBlockX()
                && otherLoc.getBlockZ() == location.getBlockZ()
                && otherLoc.getBlockY() == location.getBlockY();
    }

    public void restore(){
        //System.out.println("Init restore...");
        for (RestoreBlock rb : restoreBlocks) {
            rb.restore();
            //System.out.println("Restoring...");
        }
    }
}
