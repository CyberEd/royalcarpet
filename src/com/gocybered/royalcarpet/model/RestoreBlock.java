package com.gocybered.royalcarpet.model;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;

/**
 * Created by Eddie on 3/19/2016.
 */
public class RestoreBlock {
    private Block block;
    private BlockState state;
    private byte data;


    protected RestoreBlock(Block block){
        this.block = block;
        this.state = block.getState();
        //There is no good replacement for this
        //deprecated method, so we use it anyway
        this.data = block.getData();
    }

    protected void restore(){
        block.setType(state.getType());
        block.setData(data);
    }

    protected Location getLocation(){
        return state.getLocation();
    }
}