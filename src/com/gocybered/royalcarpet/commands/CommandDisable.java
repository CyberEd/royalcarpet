package com.gocybered.royalcarpet.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Eddie on 3/19/2016.
 */
public class CommandDisable implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            CommandEnable.stop(player);

            return true;
        }

        //Otherwise, command use is invalid
        return false;
    }
}
