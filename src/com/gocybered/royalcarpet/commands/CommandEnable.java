package com.gocybered.royalcarpet.commands;

import com.gocybered.royalcarpet.model.RestorePlayerBlocks;
import com.gocybered.royalcarpet.utils.BlockUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by Eddie on 3/19/2016.
 */
public class CommandEnable implements CommandExecutor {
    public static Set<Player> enabledUsers = new HashSet<>();
    public static Map<Player, RestorePlayerBlocks> restoreMap = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            enabledUsers.add(player);

            return true;
        }

        //Otherwise, command use is invalid
        return false;
    }

    public static void run(Player player){
        restoreBlocks(player);
        List<Block> blocks = CommandEnable.getCarpetBlocks(player);
        CommandEnable.buildCarpet(blocks);
    }

    public static void stop(Player player){
        CommandEnable.enabledUsers.remove(player);
        getRestoreMap(player).restore();
    }

    public static void stopAll(){
        for(Player p : enabledUsers){
            getRestoreMap(p).restore();
        }

        enabledUsers.clear();
    }

    public static void restoreBlocks(Player player){
        getRestoreMap(player).restore();
    }

    public static List<Block> getCarpetBlocks(Player player){
        Location location = player.getLocation();
        List<Block> blocks = new ArrayList<>();
        RestorePlayerBlocks map = getRestoreMap(player);

        //Increment map to starting location
        location.add(-1, 0, -1);

        map.setLocation(location);

        Set<Player> possibleOverlaps = isPotentialOverlap(player);
        //System.out.println("Possible overlaps length: " + possibleOverlaps.size());

        for(int x = 0; x < 3; x++){
            for(int y = 0; y < 3; y++){
                Block block = location.getBlock().getRelative(BlockFace.DOWN);

                //Only add blocks if on white-list and no other player's carpet is already there
                if(BlockUtils.isSolid(block) && (possibleOverlaps == null || !isOverlap(location, possibleOverlaps))) {
                    map.addBlock(block);
                    blocks.add(block);
                }

                //increment y location
                location.add(0, 0, 1);
            }

            //increment x location and reset y
            location.add(1, 0, -3);
        }

        //System.out.println("Block size: " + blocks.size());
        return blocks;
    }

    public static Set<Player> isPotentialOverlap(Player p){
        Set<Player> overlaps = new HashSet<>();

        Location loc = p.getLocation();

        Iterator it = restoreMap.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            Player key = (Player) pair.getKey();

            boolean ans = ((RestorePlayerBlocks)(pair.getValue())).isPotentialOverlap(loc);
            if(ans && key != p) overlaps.add(key);
        }

        if(overlaps.size() > 0) {
            return overlaps;
        }
        return null;
    }

    public static boolean isOverlap(Location l, Set<Player> possibleOverlaps){
        for(Player p : possibleOverlaps){
            boolean ans = getRestoreMap(p).isOverlap(l);
            if(ans) return true;
        }

        return false;
    }

    public static RestorePlayerBlocks getRestoreMap(Player player){
        RestorePlayerBlocks map = restoreMap.get(player);
        if(map == null){
            RestorePlayerBlocks rpb = new RestorePlayerBlocks(player);
            restoreMap.put(player, rpb);
            return rpb;
        }

        return map;
    }

    public static void buildCarpet(List<Block> blocks){
        for(Block b : blocks){
            b.setType(Material.DIAMOND_BLOCK);
        }
    }

}
