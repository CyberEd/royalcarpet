package com.gocybered.royalcarpet;

import com.gocybered.royalcarpet.commands.CommandDisable;
import com.gocybered.royalcarpet.commands.CommandEnable;
import com.gocybered.royalcarpet.listeners.PlayerMoveListener;
import com.gocybered.royalcarpet.listeners.PlayerQuitListener;
import org.bukkit.plugin.java.JavaPlugin;

import static org.bukkit.Bukkit.getServer;

/**
 * Created by Eddie on 3/19/2016.
 */
public class Main extends JavaPlugin {
    @Override
    public void onEnable(){
        //Fired when the server enables the plugin
        this.getCommand("enable").setExecutor(new CommandEnable());
        this.getCommand("disable").setExecutor(new CommandDisable());
        getServer().getPluginManager().registerEvents(new PlayerMoveListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);
    }

    @Override
    public void onDisable(){
        //Fired when the server stops and disables all plugins
        CommandEnable.stopAll();
    }
}
