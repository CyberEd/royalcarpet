package com.gocybered.royalcarpet.listeners;

import com.gocybered.royalcarpet.commands.CommandEnable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Eddie on 4/16/2016.
 */
public class PlayerQuitListener implements Listener {
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        if(player != null && CommandEnable.enabledUsers.contains(player)){
            CommandEnable.stop(player);
            //System.out.println("Removed carpet for disconnected user.");
        }
    }
}
