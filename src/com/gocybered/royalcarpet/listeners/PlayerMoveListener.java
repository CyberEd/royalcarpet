package com.gocybered.royalcarpet.listeners;

import com.gocybered.royalcarpet.commands.CommandEnable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Created by Eddie on 3/19/2016.
 */
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event){
        //Return immediately if the player hasn't moved
        if (event.getFrom().getBlockX() == event.getTo().getBlockX()
                && event.getFrom().getBlockZ() == event.getTo().getBlockZ()
                && event.getFrom().getBlockY() == event.getTo().getBlockY()) {
            return;
        }

        Player player = event.getPlayer();

        if(CommandEnable.enabledUsers.contains(player)) {
            CommandEnable.run(player);
        }
    }
}
