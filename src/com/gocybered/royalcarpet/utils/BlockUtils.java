package com.gocybered.royalcarpet.utils;

import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Eddie on 3/19/2016.
 */
public class BlockUtils {
    private static Set<Material> destructableMaterials;
    private static Set<Material> solidMaterials;

    static Set<Material> getDestructableMaterials(){
        if(destructableMaterials != null){
            return destructableMaterials;
        }

        //Else, generate list
        Set<Material> dm = new HashSet<>();
        dm.add(Material.ACACIA_DOOR);
        dm.add(Material.BIRCH_DOOR);
        dm.add(Material.DARK_OAK_DOOR);
        dm.add(Material.IRON_DOOR);
        dm.add(Material.JUNGLE_DOOR);
        dm.add(Material.SPRUCE_DOOR);
        dm.add(Material.WOODEN_DOOR);
        dm.add(Material.IRON_TRAPDOOR);
        dm.add(Material.WORKBENCH);
        dm.add(Material.CHEST);
        dm.add(Material.ANVIL);
        dm.add(Material.BREWING_STAND);
        dm.add(Material.CACTUS);
        dm.add(Material.CAULDRON);
        dm.add(Material.CLAY);
        dm.add(Material.DISPENSER);
        dm.add(Material.FURNACE);
        dm.add(Material.ENDER_CHEST);
        dm.add(Material.COMMAND_MINECART);
        dm.add(Material.MINECART);
        dm.add(Material.EXPLOSIVE_MINECART);
        dm.add(Material.HOPPER_MINECART);
        dm.add(Material.STORAGE_MINECART);
        dm.add(Material.POWERED_MINECART);
        dm.add(Material.REDSTONE);
        dm.add(Material.REDSTONE_COMPARATOR);
        dm.add(Material.REDSTONE_COMPARATOR_ON);
        dm.add(Material.REDSTONE_COMPARATOR_OFF);
        dm.add(Material.REDSTONE_TORCH_OFF);
        dm.add(Material.REDSTONE_TORCH_ON);
        dm.add(Material.REDSTONE_WIRE);
        dm.add(Material.BED);
        dm.add(Material.BED_BLOCK);
        dm.add(Material.ARMOR_STAND);
        dm.add(Material.BANNER);
        dm.add(Material.BIRCH_DOOR);
        dm.add(Material.BOAT);
        dm.add(Material.TRAP_DOOR);

        //Return
        destructableMaterials = dm;
        return dm;
    }

    static Set<Material> getSolidMaterials(){
        if(solidMaterials != null){
            return solidMaterials;
        }

        //Else, build mats list
        Set<Material> sm = new HashSet<>();
        sm.add(Material.DIAMOND_BLOCK);
        sm.add(Material.EMERALD_BLOCK);
        sm.add(Material.COAL_BLOCK);
        sm.add(Material.GOLD_BLOCK);
        sm.add(Material.IRON_BLOCK);
        sm.add(Material.MELON_BLOCK);
        sm.add(Material.LAPIS_BLOCK);
        sm.add(Material.QUARTZ_BLOCK);
        sm.add(Material.REDSTONE_BLOCK);
        sm.add(Material.CLAY);
        sm.add(Material.AIR);
        sm.add(Material.DIRT);
        sm.add(Material.STONE);
        sm.add(Material.GLOWSTONE);
        sm.add(Material.COBBLESTONE);
        sm.add(Material.SANDSTONE);
        sm.add(Material.MOSSY_COBBLESTONE);
        sm.add(Material.CARPET);
        sm.add(Material.LOG);
        sm.add(Material.LOG_2);
        sm.add(Material.GRASS);
        sm.add(Material.WOOL);
        sm.add(Material.ICE);
        sm.add(Material.PACKED_ICE);
        sm.add(Material.FROSTED_ICE);
        sm.add(Material.TNT);
        sm.add(Material.BRICK);
        sm.add(Material.CLAY_BRICK);
        sm.add(Material.NETHER_BRICK);
        sm.add(Material.SMOOTH_BRICK);
        sm.add(Material.END_BRICKS);
        sm.add(Material.ACACIA_STAIRS);
        sm.add(Material.BIRCH_WOOD_STAIRS);
        sm.add(Material.BRICK_STAIRS);
        sm.add(Material.COBBLESTONE_STAIRS);
        sm.add(Material.DARK_OAK_STAIRS);
        sm.add(Material.JUNGLE_WOOD_STAIRS);
        sm.add(Material.RED_SANDSTONE_STAIRS);
        sm.add(Material.PURPUR_STAIRS);
        sm.add(Material.SMOOTH_STAIRS);
        sm.add(Material.LAVA);
        sm.add(Material.WATER);
        sm.add(Material.STATIONARY_LAVA);
        sm.add(Material.STATIONARY_WATER);
        sm.add(Material.COAL_ORE);
        sm.add(Material.DIAMOND_ORE);
        sm.add(Material.EMERALD_ORE);
        sm.add(Material.GLOWING_REDSTONE_ORE);
        sm.add(Material.GOLD_ORE);
        sm.add(Material.IRON_ORE);
        sm.add(Material.LAPIS_ORE);
        sm.add(Material.QUARTZ_ORE);
        sm.add(Material.REDSTONE_ORE);
        sm.add(Material.LEAVES);
        sm.add(Material.LEAVES_2);

        return sm;
    }

    public static boolean isDestructable(Block b){
        Set<Material> destructableMaterials = getDestructableMaterials();

        Material m = b.getType();
        if(destructableMaterials.contains(m)){
            return true;
        }else{
            return false;
        }
    }

    public static boolean isSolid(Block b){
        Set<Material> solidMats = getSolidMaterials();

        Material m = b.getType();
        if(solidMats.contains(m)){
            return true;
        }

        return false;
    }
}
